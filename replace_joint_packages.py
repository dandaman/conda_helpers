#!/usr/bin/env python
# AUHTOR: Daniel Lang <daniellang@bundeswehr.org>

from yaml import load, dump
import re
import sys
import argparse
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


parser = argparse.ArgumentParser(description='Transfer version of joint packages between 2 conda environments in yaml format from second into first environment.')
parser.add_argument("first",metavar="FIRST",type=str,help="1rst environment yaml file")
parser.add_argument("second",metavar="SECOND",type=str,help="2nd environment yaml file")
args= parser.parse_args()

with open(args.first, 'r') as f:
    d1=load(f,Loader=Loader)

with open(args.second, 'r') as f:
    d2=load(f,Loader=Loader)

d3=d1.copy()
d3.pop("name",None)
d3.pop("prefix",None)
d3.pop("dependencies",None)

d1_pip=None
d2_pip=None

def pop_pip(deps):
    pip=None
    for i,e in enumerate(deps):
        if isinstance(e,dict) and "pip" in e:
            del deps[i]
            pip=e
    return(deps,pip)

d1["dependencies"],d1_pip=pop_pip(d1["dependencies"])
d2["dependencies"],d2_pip=pop_pip(d2["dependencies"])

def intersect_deps(first,second):
    D1=set([re.sub(r"=.+$","",x) for x in  first if isinstance(x,str)])
    D2=set([re.sub(r"=.+$","",x) for x in  second if isinstance(x,str) ])
    return(D1.intersection(D2))

D=intersect_deps(d1["dependencies"],d2["dependencies"])
new=[x for x in  d2["dependencies"] if isinstance(x,str) and re.sub(r"=.+$","",x) in D ]
keep=[x for x in  d1["dependencies"] if not re.sub(r"=.+$","",x) in D ]

D_pip=None
keep_pip=None
if d1_pip and d2_pip:
    D_pip=intersect_deps(d1_pip,d2_pip)
    if D_pip:
        new_pip=[x for x in  d2_pip if isinstance(x,str) and re.sub(r"=.+$","",x) in D_pip ]
        if d1_pip:
            keep_pip=[x for x in  d1_pip if not re.sub(r"=.+$","",x) in D_pip ]
        if keep_pip:
            keep_pip=list(set(keep_pip).union(set(new_pip)))
else:
    if d1_pip:
            keep_pip=d1_pip
    
d3["dependencies"]=list(set(keep).union(set(new)))

if keep_pip:
    d3["dependencies"].append({"pip":keep_pip})

dump(d3,sys.stdout)
